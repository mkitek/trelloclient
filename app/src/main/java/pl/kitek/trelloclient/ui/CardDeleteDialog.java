package pl.kitek.trelloclient.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.squareup.otto.Bus;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.events.CardDeleteFailEvent;
import pl.kitek.trelloclient.events.CardDeleteSuccessEvent;
import pl.kitek.trelloclient.events.TodoEvent;
import pl.kitek.trelloclient.models.TodoCard;

public class CardDeleteDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String TODO_CARD = "TodoCard";

    @Inject TodoCardManager todoCardManager;
    @Inject Bus bus;

    private TodoCard card;

    public static CardDeleteDialog newInstance(TodoCard todoCard) {
        CardDeleteDialog dialog = new CardDeleteDialog();
        if (todoCard != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(TODO_CARD, todoCard);
            dialog.setArguments(bundle);
        }

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.inject(this);

        Bundle arguments = getArguments();
        if (null != arguments) {
            card = arguments.getParcelable(TODO_CARD);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.are_u_sure))
                .setPositiveButton(getString(R.string.delete), this)
                .setNegativeButton(getString(R.string.cancel), null);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        TodoEvent event;
        try {
            todoCardManager.markAsRemoved(card);
            event = new CardDeleteSuccessEvent(card);
        } catch (SQLException e) {
            event = new CardDeleteFailEvent(card);
            e.printStackTrace();
        }
        bus.post(event);
    }
}
