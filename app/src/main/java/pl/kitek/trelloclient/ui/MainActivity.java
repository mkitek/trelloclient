package pl.kitek.trelloclient.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.adapters.ListAdapter;
import pl.kitek.trelloclient.db.TodoListManager;
import pl.kitek.trelloclient.events.SyncFailEvent;
import pl.kitek.trelloclient.events.SyncSuccessEvent;
import pl.kitek.trelloclient.models.TodoList;
import pl.kitek.trelloclient.sync.TrelloService;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Inject TodoListManager todoListManager;
    @Inject Bus bus;

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.slidingTabs) TabLayout tabLayout;
    @Bind(R.id.viewPager) ViewPager viewPager;
    @Bind(R.id.panelLayout) CoordinatorLayout panelLayout;

    private MenuItem menuItem;
    private ListAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        App.inject(this);

        setupView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onClick(View v) {
        refresh();
    }

    private void setupView() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupPager();
    }

    private void setupPager() {
        try {
            List<TodoList> items = todoListManager.getAll();
            if (pagerAdapter == null) {
                pagerAdapter = new ListAdapter(getSupportFragmentManager(), items);
                viewPager.setAdapter(pagerAdapter);
            } else {
                pagerAdapter.swapItems(items);
            }

            if (items.size() > 0) {
                tabLayout.setupWithViewPager(viewPager);
                tabLayout.setVisibility(View.VISIBLE);
            } else {
                tabLayout.setVisibility(View.GONE);
                // TODO: 18.08.15 Show empty view?
            }
        } catch (SQLException e) {
            Timber.e(e, "setupView ERROR");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menuItem = menu.findItem(R.id.action_refresh);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        startLoadingAnim();
        startService(new Intent(this, TrelloService.class));
    }

    private void startLoadingAnim() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            try {
                Animation rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);

                int dp = 12;
                int px = (int) (dp * Resources.getSystem().getDisplayMetrics().density);

                ImageView image = new ImageView(this);
                image.setImageResource(R.drawable.ic_autorenew_white);
                image.setPadding(px, 0, px, 0);
                image.startAnimation(rotate);

                menuItem.setActionView(image);
            } catch (Exception ignored) {
            }
        }
    }

    private void stopLoadingAnim() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            try {
                menuItem.getActionView().setAnimation(null);
                menuItem.setActionView(null);
            } catch (Exception ignored) {
            }
        }
    }

    private void showSnackBar(String message, @Nullable String actionText, @Nullable View.OnClickListener listener) {
        Snackbar snackbar = Snackbar.make(panelLayout, message, Snackbar.LENGTH_LONG);
        TextView snackTextView = (TextView) snackbar.getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        snackTextView.setTextColor(Color.WHITE);

        if (null != actionText && null != listener) {
            snackbar.setAction(actionText, listener);
            snackbar.setActionTextColor(Color.YELLOW);
        }

        snackbar.show();
    }

    // Events

    @Subscribe
    public void onSyncSuccess(SyncSuccessEvent event) {
        stopLoadingAnim();
        setupPager();
        showSnackBar(getString(R.string.refreshed), null, null);
    }

    @Subscribe
    public void onSyncFail(SyncFailEvent event) {
        stopLoadingAnim();
        showSnackBar(getString(R.string.error_occurred), getString(R.string.retry), this);
    }

}
