package pl.kitek.trelloclient.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.events.LoginFailEvent;
import pl.kitek.trelloclient.events.LoginSuccessEvent;
import pl.kitek.trelloclient.managers.AuthManager;
import pl.kitek.trelloclient.sync.TrelloApi;
import pl.kitek.trelloclient.sync.TrelloWebViewClient;

public class LoginActivity extends AppCompatActivity {

    @Inject Bus bus;
    @Inject AuthManager authManager;

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.webView) WebView webview;
    @Bind(R.id.slidingTabs) TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        App.inject(this);

        if (checkToken()) {
            return;
        }

        setupView();

        if (null == savedInstanceState) {
            webview.loadUrl(TrelloApi.OAUTH_URL);
        }
    }

    private boolean checkToken() {
        if (authManager.hasToken()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
            return true;
        }
        return false;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupView() {
        ButterKnife.bind(this);

        tabLayout.setVisibility(View.GONE);
        setSupportActionBar(toolbar);

        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new TrelloWebViewClient());
    }

    @Override
    protected void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        webview.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        webview.restoreState(savedInstanceState);
    }

    // Events

    @Subscribe
    public void onLoginSuccessEvent(LoginSuccessEvent event) {
        checkToken();
    }

    @Subscribe
    public void onLoginFailEvent(LoginFailEvent event) {
        Toast.makeText(this, getString(R.string.error_occurred), Toast.LENGTH_LONG).show();
    }
}
