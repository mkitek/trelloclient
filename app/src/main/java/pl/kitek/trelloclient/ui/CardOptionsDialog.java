package pl.kitek.trelloclient.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.models.TodoCard;
import timber.log.Timber;

public class CardOptionsDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private static final String TODO_CARD = "TodoCard";

    private static final int EDIT = 0;
    private static final int MOVE = 1;
    private static final int DELETE = 2;

    private TodoCard card;

    public static CardOptionsDialog newInstance(TodoCard todoCard) {
        CardOptionsDialog dialog = new CardOptionsDialog();
        if (todoCard != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(TODO_CARD, todoCard);
            dialog.setArguments(bundle);
        }

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            card = arguments.getParcelable(TODO_CARD);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(card.getName())
                .setItems(R.array.card_options, this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Timber.d("onClick " + which);

        switch (which) {
            case EDIT:
                break;
            case MOVE:
                CardMoveDialog moveCardDialog = CardMoveDialog.newInstance(card);
                moveCardDialog.show(getActivity().getSupportFragmentManager(), "moveCardDialog");
                break;
            case DELETE:
                CardDeleteDialog cardDeleteDialog = CardDeleteDialog.newInstance(card);
                cardDeleteDialog.show(getActivity().getSupportFragmentManager(), "deleteCardDialog");
                break;
        }
    }
}
