package pl.kitek.trelloclient.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.squareup.otto.Bus;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.db.TodoListManager;
import pl.kitek.trelloclient.events.CardMoveSuccessEvent;
import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.models.TodoList;

public class CardMoveDialog extends DialogFragment implements DialogInterface.OnClickListener {

    @Inject TodoListManager todoListManager;
    @Inject TodoCardManager todoCardManager;
    @Inject Bus bus;

    private static final String TODO_CARD = "TodoCard";
    private TodoCard card;
    private List<TodoList> todoLists;

    public static CardMoveDialog newInstance(TodoCard todoCard) {
        CardMoveDialog dialog = new CardMoveDialog();
        if (todoCard != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(TODO_CARD, todoCard);
            dialog.setArguments(bundle);
        }
        return dialog;
    }

    public CardMoveDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.inject(this);

        Bundle arguments = getArguments();
        if (null != arguments) {
            card = arguments.getParcelable(TODO_CARD);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.move_to_list));

        try {
            todoLists = todoListManager.getAll();
            int size = todoLists.size();
            String[] names = new String[size];
            for (int i = 0; i < size; i++) {
                names[i] = todoLists.get(i).getName();
            }
            builder.setItems(names, this);

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        try {
            todoCardManager.move(card, todoLists.get(which));
            bus.post(new CardMoveSuccessEvent());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
