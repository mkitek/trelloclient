package pl.kitek.trelloclient.events;

import pl.kitek.trelloclient.models.TodoCard;

public class CardDeleteSuccessEvent implements TodoEvent {

    private final TodoCard card;

    public CardDeleteSuccessEvent(TodoCard card) {
        this.card = card;
    }

    public TodoCard getCard() {
        return card;
    }
}
