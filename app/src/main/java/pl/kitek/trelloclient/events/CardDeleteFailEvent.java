package pl.kitek.trelloclient.events;

import pl.kitek.trelloclient.models.TodoCard;

public class CardDeleteFailEvent implements TodoEvent {

    private final TodoCard card;

    public CardDeleteFailEvent(TodoCard card) {
        this.card = card;
    }

    public TodoCard getCard() {
        return card;
    }
}
