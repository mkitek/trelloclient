package pl.kitek.trelloclient.managers;

import android.content.SharedPreferences;
import android.text.TextUtils;

import pl.kitek.trelloclient.sync.TrelloApi;

public class AuthManager {

    private final SharedPreferences sharedPreferences;

    public AuthManager(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setToken(String token) {
        sharedPreferences.edit().putString(TrelloApi.TOKEN_KEY, token).apply();
    }

    public String getToken() {
        return sharedPreferences.getString(TrelloApi.TOKEN_KEY, null);
    }

    public boolean hasToken() {
        return !TextUtils.isEmpty(getToken());
    }

    public void clearToken() {
        sharedPreferences.edit().remove(TrelloApi.TOKEN_KEY).apply();
    }

}
