package pl.kitek.trelloclient;

import android.app.Application;

import dagger.ObjectGraph;
import timber.log.Timber;

public class App extends Application {

    public static String APP_URI;
    public static String APP_NAME;

    private static ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        APP_URI = "app://" + getApplicationContext().getPackageName();
        APP_NAME = getString(R.string.app_name);

        objectGraph = ObjectGraph.create(new MainModuleAdapter(getApplicationContext()));
    }

    public static void inject(Object object) {
        objectGraph.inject(object);
    }
}
