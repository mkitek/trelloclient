package pl.kitek.trelloclient.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "todo_card")
public class TodoCard implements Parcelable {

    public static final String ID = "id";
    public static final String ID_LIST = "id_list";
    public static final String NAME = "name";
    public static final String DESC = "desc";
    public static final String POS = "pos";
    public static final String IS_REMOVED = "is_removed";

    @Expose @DatabaseField(id = true, columnName = ID)
    private String id;

    @Expose @DatabaseField(canBeNull = false, columnName = ID_LIST)
    private String idList;

    @Expose @DatabaseField(canBeNull = false, columnName = NAME)
    private String name;

    @Expose @DatabaseField(canBeNull = true, columnName = DESC)
    private String desc;

    @Expose @DatabaseField(canBeNull = false, columnName = POS)
    private int pos;

    @Expose @DatabaseField(canBeNull = false, columnName = IS_REMOVED, defaultValue = "false")
    private boolean isRemoved;

    private int adapterPosition = -1;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdList() {
        return idList;
    }

    public void setIdList(String idList) {
        this.idList = idList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setIsRemoved(boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public TodoCard() {
    }

    protected TodoCard(Parcel in) {
        id = in.readString();
        idList = in.readString();
        name = in.readString();
        desc = in.readString();
        pos = in.readInt();
        adapterPosition = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(idList);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeInt(pos);
        dest.writeInt(adapterPosition);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TodoCard> CREATOR = new Creator<TodoCard>() {
        @Override
        public TodoCard createFromParcel(Parcel in) {
            return new TodoCard(in);
        }

        @Override
        public TodoCard[] newArray(int size) {
            return new TodoCard[size];
        }
    };
}
