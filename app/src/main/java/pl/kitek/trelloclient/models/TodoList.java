package pl.kitek.trelloclient.models;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "todo_list")
public class TodoList {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String CLOSED = "closed";
    public static final String ID_BOARD = "id_board";
    public static final String POS = "pos";

    @Expose @DatabaseField(id = true, columnName = ID)
    private String id;

    @Expose @DatabaseField(canBeNull = false, columnName = NAME)
    private String name;

    @Expose @DatabaseField(canBeNull = false, columnName = CLOSED)
    private boolean closed;

    @Expose @DatabaseField(canBeNull = false, columnName = ID_BOARD)
    private String idBoard;

    @Expose @DatabaseField(canBeNull = false, columnName = POS)
    private int pos;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getIdBoard() {
        return idBoard;
    }

    public void setIdBoard(String idBoard) {
        this.idBoard = idBoard;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "TodoList { 'id': " + getId() + ", name: '" + getName() + "' }";
    }
}
