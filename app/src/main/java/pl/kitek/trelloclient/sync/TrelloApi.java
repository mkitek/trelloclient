package pl.kitek.trelloclient.sync;

import java.util.List;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.models.TodoList;
import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Path;

public interface TrelloApi {

    String TOKEN_KEY = "token";
    String BOARD_ID = "N1GSWXIq"; // TODO: 18.08.15 Mozna pobierac przez api i dac userowi mozliwosc wyboru
    String API_URL = "https://api.trello.com/1/";
    String API_KEY = "0538f27f1e115986d747a7192a443ce9";
    String OAUTH_URL = API_URL + "authorize?return_url=" + App.APP_URI
            + "&callback_method=fragment&expiration=never&scope=read,write&name=" + App.APP_NAME
            + "&key=" + API_KEY;

    @GET("/board/{board}/lists")
    List<TodoList> getLists(@Path("board") String boardId);

    @GET("/lists/{list}/cards")
    List<TodoCard> getCards(@Path("list") String listId);

    @DELETE("/cards/{id}")
    Response deleteCard(@Path("id") String cardId);

}
