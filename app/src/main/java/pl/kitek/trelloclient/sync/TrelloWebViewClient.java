package pl.kitek.trelloclient.sync;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.squareup.otto.Bus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.events.LoginFailEvent;
import pl.kitek.trelloclient.events.LoginSuccessEvent;
import pl.kitek.trelloclient.events.TodoEvent;
import pl.kitek.trelloclient.managers.AuthManager;

public class TrelloWebViewClient extends WebViewClient {

    @Inject Bus bus;
    @Inject AuthManager authManager;

    private final Pattern pattern;

    public TrelloWebViewClient() {
        App.inject(this);
        pattern = Pattern.compile("#token=([a-z0-9]+)");
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        if (url.startsWith(App.APP_URI)) {
            TodoEvent event = new LoginFailEvent();

            Matcher matcher = pattern.matcher(url);
            if (matcher.find()) {
                String token = matcher.group(1);
                if (token != null) {
                    authManager.setToken(token);
                    event = new LoginSuccessEvent();
                }
            }

            bus.post(event);
            return true;
        }

        // TODO: 18.08.15 Wykrywac i blokowac URL poza proces logowania

        return false;
    }
}
