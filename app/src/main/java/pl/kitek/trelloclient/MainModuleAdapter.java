package pl.kitek.trelloclient;

import android.content.Context;
import android.content.SharedPreferences;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.kitek.trelloclient.db.DatabaseHelper;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.db.TodoListManager;
import pl.kitek.trelloclient.fragments.CardListFragment;
import pl.kitek.trelloclient.managers.AuthManager;
import pl.kitek.trelloclient.sync.TrelloApi;
import pl.kitek.trelloclient.sync.TrelloService;
import pl.kitek.trelloclient.sync.TrelloWebViewClient;
import pl.kitek.trelloclient.ui.CardDeleteDialog;
import pl.kitek.trelloclient.ui.CardMoveDialog;
import pl.kitek.trelloclient.ui.LoginActivity;
import pl.kitek.trelloclient.ui.MainActivity;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

@Module(injects = {
        MainActivity.class,
        LoginActivity.class,

        CardListFragment.class,
        CardMoveDialog.class,
        CardDeleteDialog.class,

        TrelloService.class,
        TrelloWebViewClient.class
}, library = true)
public class MainModuleAdapter {

    private final Context context;

    public MainModuleAdapter(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Singleton
    @Provides
    public Bus provideBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    public DatabaseHelper provideDatabaseHelper(Context context) {
        return new DatabaseHelper(context);
    }

    @Provides
    @Singleton
    public TodoListManager provideTodoListManager(DatabaseHelper db) {
        return new TodoListManager(db);
    }

    @Provides
    @Singleton
    public TodoCardManager provideTodoCardManager(DatabaseHelper db) {
        return new TodoCardManager(db);
    }

    @Singleton
    @Provides
    public AuthManager provideAuthManager(SharedPreferences sharedPreferences) {
        return new AuthManager(sharedPreferences);
    }

    @Singleton
    @Provides
    public SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("trelloClient", Context.MODE_PRIVATE);
    }

    @Singleton
    @Provides
    public TrelloApi provideTrelloApi(final AuthManager authManager) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(TrelloApi.API_URL)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addQueryParam("key", TrelloApi.API_KEY);
                        if (authManager.hasToken()) {
                            request.addQueryParam("token", authManager.getToken());
                        }
                    }
                })
                .build();

        return restAdapter.create(TrelloApi.class);
    }

}
