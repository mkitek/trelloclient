package pl.kitek.trelloclient.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.sql.SQLException;

import javax.inject.Inject;

import pl.kitek.trelloclient.App;
import pl.kitek.trelloclient.R;
import pl.kitek.trelloclient.adapters.CardAdapter;
import pl.kitek.trelloclient.db.TodoCardManager;
import pl.kitek.trelloclient.events.CardDeleteFailEvent;
import pl.kitek.trelloclient.events.CardDeleteSuccessEvent;
import pl.kitek.trelloclient.events.CardMoveSuccessEvent;
import pl.kitek.trelloclient.models.TodoCard;
import pl.kitek.trelloclient.ui.CardOptionsDialog;
import timber.log.Timber;

public class CardListFragment extends Fragment implements View.OnClickListener {

    private static final String ID = "pl.kitek.trelloclient.fragments.CardListFragment.ID";

    private CardAdapter cardAdapter;
    private String listId;

    @Inject TodoCardManager todoCardManager;
    @Inject Bus bus;

    public static CardListFragment newInstance(String listId) {
        Bundle bundle = new Bundle();
        bundle.putString(ID, listId);

        CardListFragment cardListFragment = new CardListFragment();
        cardListFragment.setArguments(bundle);

        return cardListFragment;
    }

    public void refresh() {
        Timber.d("refresh ");
        try {
            cardAdapter.swapCards(todoCardManager.getAll(listId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.inject(this);

        Bundle arguments = getArguments();
        if (arguments != null) {
            listId = arguments.getString(ID);
            if (listId != null) {
                try {
                    cardAdapter = new CardAdapter(todoCardManager.getAll(listId));
                    cardAdapter.setMoreBtnClickListener(this);
                } catch (SQLException e) {
                    Timber.d("onCreate ERROR: " + e.getMessage());
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_card_list, container, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(cardAdapter);

        return recyclerView;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        TodoCard item = cardAdapter.getItem(position);
        item.setAdapterPosition(position);

        CardOptionsDialog optionsDialog = CardOptionsDialog.newInstance(item);
        optionsDialog.show(getActivity().getSupportFragmentManager(), "cardOptionsDialog");
    }

    // Events

    @Subscribe
    public void onCardDeleteSuccess(CardDeleteSuccessEvent event) {
        TodoCard card = event.getCard();
        if (card == null || !listId.equals(card.getIdList())) {
            return;
        }
        if (card.getAdapterPosition() > -1) {
            cardAdapter.removeCard(card);
        }
    }

    @Subscribe
    public void onCardDeleteFail(CardDeleteFailEvent event) {
        // TODO: 24.08.15 implement me
    }

    @Subscribe
    public void onCardMoveSuccess(CardMoveSuccessEvent event) {
        // jezeli karta jest z mojej listy to ja usun

        // jezeli karta ma trafic na moja liste to ja dodaj

        Timber.d("onCardMoveSuccess ");
    }

}
