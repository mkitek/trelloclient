package pl.kitek.trelloclient.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import pl.kitek.trelloclient.fragments.CardListFragment;
import pl.kitek.trelloclient.models.TodoList;
import timber.log.Timber;

public class ListAdapter extends FragmentPagerAdapter {

    @NonNull private List<TodoList> items;

    public ListAdapter(FragmentManager fm, @NonNull List<TodoList> items) {
        super(fm);
        this.items = items;
    }

    public void swapItems(@NonNull List<TodoList> items) {
        this.items = items;
        notifyDataSetChanged();
        //
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Fragment getItem(int position) {
        String id = items.get(position).getId();
        return CardListFragment.newInstance(id);
    }

    @Override
    public int getItemPosition(Object object) {
        Timber.d("getItemPosition ");
        if (object instanceof CardListFragment) {
            ((CardListFragment) object).refresh();
        }

        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return items.get(position).getName();
    }
}
